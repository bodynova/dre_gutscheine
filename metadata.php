<?php
/**
 *	dre_Gutscheine
 **/

$sMetadataVersion = '2.0';

$aModule          = [
    'id'          => 'dre_gutscheine',
    'title'       => '<img src="../modules/bender/dre_gutscheine/out/img/favicon.ico" height="20px" title="Bender Gutschein Modul">ody Gutschein Modul',
    'description' => '',
    'thumbnail'   => 'out/img/logo_bodynova.png',
    'version'     => '2.0.0',
    'author'      => 'Bodynova GmbH',
    'email'       => 'support@bodynova.de',
    'url'         => 'https://bodynova.de',
    'controllers' => [
        'dre_voucherserie_export'   =>
            \Bender\dre_Gutscheine\Application\Controller\Admin\dre_voucherserie_export::class,
        'dre_voucherserie_generate' =>
            \Bender\dre_Gutscheine\Application\Controller\Admin\dre_voucherserie_generate::class,
        'dre_voucherserie_main' =>
            \Bender\dre_Gutscheine\Application\Controller\Admin\dre_voucherserie_main::class
    ],
    'extend'      => [
        \OxidEsales\Eshop\Application\Model\Voucher::class => \Bender\dre_Gutscheine\Application\Model\dre_Voucher::class,
    ],
    'templates'   => [
        'dre_voucherserie_export.tpl'   =>
            'bender/dre_gutscheine/Application/views/admin/tpl/dre_voucherserie_export.tpl',
        'dre_voucherserie_generate.tpl' =>
            'bender/dre_gutscheine/Application/views/admin/tpl/dre_voucherserie_generate.tpl',
        'dre_voucherserie_main.tpl' =>
            'bender/dre_gutscheine/Application/views/admin/tpl/dre_voucherserie_main.tpl'
    ],
    'blocks'      => [
    ],
    'settings'    => [
    ]
];
