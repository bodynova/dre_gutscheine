<?php

namespace Bender\dre_Gutscheine\Application\Model;

class dre_Voucher extends dre_Voucher_parent
{
    /**
     * Returns basket item information (id,amount,price) array takig item list from order.
     *
     * @param \OxidEsales\Eshop\Application\Model\Discount $oDiscount discount object
     *
     * @return array
     * @deprecated underscore prefix violates PSR12, will be renamed to "getOrderBasketItems" in next major
     */
    protected function _getOrderBasketItems($oDiscount = null) // phpcs:ignore PSR2.Methods.MethodDeclaration.Underscore
    {
        if (is_null($oDiscount)) {
            $oDiscount = $this->_getSerieDiscount();
        }

        $oOrder = oxNew(\OxidEsales\Eshop\Application\Model\Order::class);
        $oOrder->load($this->oxvouchers__oxorderid->value);

        $aItems = [];
        $iCount = 0;

        $oSeries = $this->getSerie();
        if (!$oSeries->oxvoucherseries__oxnoskipdiscount->value) {
            foreach ($oOrder->getOrderArticles(true) as $oOrderArticle) {
                if (!$oOrderArticle->skipDiscounts() && $oDiscount->isForBasketItem($oOrderArticle)) {
                    $aItems[$iCount] = [
                        'oxid' => $oOrderArticle->getProductId(),
                        'price' => $oOrderArticle->oxorderarticles__oxbprice->value,
                        'discount' => $oDiscount->getAbsValue($oOrderArticle->oxorderarticles__oxbprice->value),
                        'amount' => $oOrderArticle->oxorderarticles__oxamount->value,
                    ];
                    $iCount++;
                }
            }
        }else{
            foreach ($oOrder->getOrderArticles(true) as $oOrderArticle) {
                if ($oDiscount->isForBasketItem($oOrderArticle)) {
                    $aItems[$iCount] = [
                        'oxid' => $oOrderArticle->getProductId(),
                        'price' => $oOrderArticle->oxorderarticles__oxbprice->value,
                        'discount' => $oDiscount->getAbsValue($oOrderArticle->oxorderarticles__oxbprice->value),
                        'amount' => $oOrderArticle->oxorderarticles__oxamount->value,
                    ];
                    $iCount++;
                }
            }
        }

        return $aItems;
    }

    /**
     * Returns basket item information (id,amount,price) array taking item list from session.
     *
     * @param \OxidEsales\Eshop\Application\Model\Discount $oDiscount discount object
     *
     * @return array
     * @deprecated underscore prefix violates PSR12, will be renamed to "getSessionBasketItems" in next major
     */
    protected function _getSessionBasketItems($oDiscount = null) // phpcs:ignore PSR2.Methods.MethodDeclaration.Underscore
    {
        if (is_null($oDiscount)) {
            $oDiscount = $this->_getSerieDiscount();
        }

        $oBasket = $this->getSession()->getBasket();
        $aItems = [];
        $iCount = 0;

        $oSeries = $this->getSerie();
        if(!$oSeries->oxvoucherseries__oxnoskipdiscount->value){
            foreach ($oBasket->getContents() as $oBasketItem) {
                if (!$oBasketItem->isDiscountArticle() && ($oArticle = $oBasketItem->getArticle()) && !$oArticle->skipDiscounts() && $oDiscount->isForBasketItem($oArticle)) {
                    $aItems[$iCount] = [
                        'oxid' => $oArticle->getId(),
                        'price' => $oArticle->getBasketPrice($oBasketItem->getAmount(), $oBasketItem->getSelList(), $oBasket)->getPrice(),
                        'discount' => $oDiscount->getAbsValue($oArticle->getBasketPrice($oBasketItem->getAmount(), $oBasketItem->getSelList(), $oBasket)->getPrice()),
                        'amount' => $oBasketItem->getAmount(),
                    ];
                    $iCount++;
                }
            }
        }else{
            foreach ($oBasket->getContents() as $oBasketItem) {
                if (!$oBasketItem->isDiscountArticle() && ($oArticle = $oBasketItem->getArticle()) && $oDiscount->isForBasketItem($oArticle)) {
                    $aItems[$iCount] = [
                        'oxid' => $oArticle->getId(),
                        'price' => $oArticle->getBasketPrice($oBasketItem->getAmount(), $oBasketItem->getSelList(), $oBasket)->getPrice(),
                        'discount' => $oDiscount->getAbsValue($oArticle->getBasketPrice($oBasketItem->getAmount(), $oBasketItem->getSelList(), $oBasket)->getPrice()),
                        'amount' => $oBasketItem->getAmount(),
                    ];

                    $iCount++;
                }
            }
        }

        return $aItems;
    }
}
