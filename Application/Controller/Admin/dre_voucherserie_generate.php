<?php
namespace Bender\dre_Gutscheine\Application\Controller\Admin;

use OxidEsales\Eshop\Core\Field;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Request;

/**
 * Voucher generating class.
 */
class dre_voucherserie_generate extends \OxidEsales\Eshop\Application\Controller\Admin\VoucherSerieMain { //\OxidEsales\Eshop\Application\Controller\Admin\AdminDetailsController { //oxAdminDetails {

    /**
     * Voucher generator class name
     *
     * @var string
     */
    public $sClassDo = "dre_voucherserie_generate";

    /**
     * used admin template
     * @var String 
     */
    protected $_sThisTemplate = "dre_voucherserie_generate.tpl";

    public function render()
    {
        return parent::render();
    }

    /**
     * Generates vouchercodes from array
     */
    public function generateVoucherBatch() {
        //get vouchercodes from textarea
        $oRequest = Registry::getRequest();
        $sVouchercodes = $oRequest->getRequestParameter("voucher_codes");
        //transform to array
        $aVouchercodes = explode("\n", $sVouchercodes);
        if (\is_array($aVouchercodes) && \count($aVouchercodes) > 0) {
            $sVoucherserie = $oRequest->getRequestParameter("oxid");
            if ($sVoucherserie) {
                $iCount = 0;
                foreach ($aVouchercodes as $sCode) {
                    if ($sCode !== '' && \strlen($sCode) <= 255) {
                        //add new voucher
                        $oNewVoucher = oxNew(\OxidEsales\Eshop\Application\Model\Voucher::class);
                        $oNewVoucher->oxvouchers__oxvoucherserieid = new Field($sVoucherserie);
                        $oNewVoucher->oxvouchers__oxvouchernr = new Field( trim($sCode) );
                        $oNewVoucher->save();
                        $iCount++;
                    }
                }
                $oLang = Registry::getLang();
                $this->_aViewData["sGenerate_Success_Message"] = $iCount++ . $oLang->translateString('DRE_VOUCHER_GENERATED'); //" Codes erfolgreich generiert!";
            }
        }
    }

    /**
     * generates vouchers with specified characters
     */
    public function generateVoucherRandom() {
        $oRequest = Registry::getRequest();
        $iVoucherCount = $oRequest->getRequestParameter("voucher_count");
        if (!$iVoucherCount) {
            $iVoucherCount = 1;
        }

        $iVoucherLength = $oRequest->getRequestParameter("voucher_length");
        $sVoucherserie = $oRequest->getRequestParameter("oxid");
        $sCharacters = $oRequest->getRequestParameter("voucher_characters");
        $sVoucherPrefix = $oRequest->getRequestParameter("voucher_prefix");
        if ($sCharacters && $iVoucherLength && $sVoucherserie) {
            $iCount = 0;
            for ($i = 0; $i < $iVoucherCount; $i++) {
                $sCode = $sVoucherPrefix . $this->_generateRandomString($iVoucherLength, $sCharacters);
                if ($sCode !== '' && \strlen($sCode) <= 255) {
                    //add new voucher
                    $oNewVoucher = oxNew(\OxidEsales\Eshop\Application\Model\Voucher::class);
                    $oNewVoucher->oxvouchers__oxvoucherserieid = new Field($sVoucherserie);
                    $oNewVoucher->oxvouchers__oxvouchernr = new Field($sCode);
                    $oNewVoucher->save();
                    $iCount++;
                }
            }
            $oLang = Registry::getLang();
            $this->_aViewData["sGenerate_Random_Success_Message"] = $iCount++ . $oLang->translateString('DRE_VOUCHER_GENERATED'); //" Codes erfolgreich generiert!";
        }
    }

    /**
     * generates random string
     */
    protected function _generateRandomString($iLength, $sCharacters) {
        $string = '';
        for ($p = 0; $p < $iLength; $p++) {
            $string .= $sCharacters[random_int(0, \strlen($sCharacters)-1)];
        }
        return $string;
    }
}