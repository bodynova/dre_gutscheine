<?php

namespace Bender\dre_Gutscheine\Application\Controller\Admin;

use OxidEsales\Eshop\Core\Registry;

/**
 * Voucher generating class.
 */
class dre_voucherserie_export extends \OxidEsales\Eshop\Application\Controller\Admin\AdminDetailsController { //oxAdminDetails {

    /**
     * used admin template
     * @var String
     */
    protected $_sThisTemplate = "dre_voucherserie_export.tpl";

    protected $_sSeperator = ";";

    /**
     * Export file extension
     *
     * @var string
     */
    public $sExportFileType = "csv";

    public function exportUsedVouchers(){
        $oRequest = Registry::getRequest();
        $sVoucherserie = $oRequest->getRequestParameter("oxid");
        $aData = $this->_getUsedVouchers($sVoucherserie);
        // if (isset($aData) && is_array($aData) && count($aData) > 0) {
        if ($aData !== null && \is_array($aData) && \count($aData) > 0) {
            $this->_generateCsvFile($aData);
        }else{
            $oLang = Registry::getLang();
            $this->_aViewData["sMessage"] = $oLang->translateString('DRE_VOUCHER_NO_ORDERS'); //"Keine Bestellungen mit Gutscheinen vorhanden!";
        }
    }

    protected function _getUsedVouchers($sVoucherserie){
        $aData = array();
        $sVoucherView = getViewName("oxvouchers");
        $sOrderView = getViewName("oxorder");
        $sSelect = "Select v.oxvouchernr, o.oxordernr, o.oxorderdate, o.oxbillcompany, o.oxbillfname, o.oxbilllname, o.oxbillemail, o.oxbillstreet, o.oxbillstreetnr, o.oxbillzip, o.oxbillcity, o.oxtotalordersum, v.oxdiscount, o.oxtotalbrutsum "
                . "from $sVoucherView v left join $sOrderView o on v.oxorderid = o.oxid "
                . "where v.oxvoucherserieid = '$sVoucherserie' and v.oxorderid != ''";


        $resultSet = \OxidEsales\Eshop\Core\DatabaseProvider::getDb()->select($sSelect);
        $allResults = $resultSet->fetchAll();
        foreach($allResults as $row) {
            $aData[] = $row;
        }
        return $aData;
    }

    protected function _generateCsvFile($aData){
        $sCSVLine = "Gutschein-Code;Bestellnummer;Bestelldatum;Firma;Vorname;Nachname;Email;Straße;Hausnummer;PLZ;Ort;Artikelsumme (nicht rabattiert);Bestellsumme;Gutscheinwert;\n";
        foreach($aData as $sDataLine){
            $sCSVLine .= $sDataLine['oxvouchernr'] . $this->_sSeperator;
            $sCSVLine .= $sDataLine['oxordernr'] . $this->_sSeperator;
            $sCSVLine .= $sDataLine['oxorderdate'] . $this->_sSeperator;
            $sCSVLine .= $sDataLine['oxbillcompany'] . $this->_sSeperator;
            $sCSVLine .= $sDataLine['oxbillfname'] . $this->_sSeperator;
            $sCSVLine .= $sDataLine['oxbilllname'] . $this->_sSeperator;
            $sCSVLine .= $sDataLine['oxbillemail'] . $this->_sSeperator;
            $sCSVLine .= $sDataLine['oxbillstreet'] . $this->_sSeperator;
            $sCSVLine .= $sDataLine['oxbillstreetnr'] . $this->_sSeperator;
            $sCSVLine .= $sDataLine['oxbillzip'] . $this->_sSeperator;
            $sCSVLine .= $sDataLine['oxbillcity'] . $this->_sSeperator;
            $sCSVLine .= $sDataLine['oxtotalbrutsum'] . $this->_sSeperator;
            $sCSVLine .= $sDataLine['oxtotalordersum'] . $this->_sSeperator;
            $sCSVLine .= $sDataLine['oxdiscount'] . $this->_sSeperator;
            $sCSVLine .= "\n";
        }
        // output as csv file
        header("Content-Type: text/csv");
        header("charset=UTF-8");
        header("Content-Length: " . strlen($sCSVLine));
        header("Content-Disposition: attachment; filename=voucher_export.csv");
        print $sCSVLine;
        exit();
    }
}
