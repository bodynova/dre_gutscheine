<?php

namespace Bender\dre_Gutscheine\Application\Controller\Admin;

class dre_voucherserie_main extends \OxidEsales\Eshop\Application\Controller\Admin\VoucherSerieMain
{
    protected $_sThisTemplate = "dre_voucherserie_main.tpl";

    public function render()
    {
        parent::render();

        return $this->_sThisTemplate;
    }
}
