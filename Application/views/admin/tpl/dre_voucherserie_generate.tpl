[{include file="headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign}]

[{assign var="readonly" value=""}]
[{if $readonly}]
    [{assign var="readonly" value="readonly disabled"}]
[{/if}]

[{*include file="headitem.tpl" box="none "
    meta_refresh_sec=$refresh
    meta_refresh_url=$oViewConf->getSelfLink()|cat:"&cl=`$sClassDo`&iStart=`$iStart`&fnc=run"
}]
[{assign var='blShowStatus' value=true}]
*}]


<script type="text/javascript">

<!--
function changeFnc( fncName )
{
    "use strict";
    console.log(fncName);
    //event.stopImmediatePropagation();
    //event.preventDefault();
    //event.preventDefault();
    //event.stopPropagation();
    console.log(document.myedit);

    //var test = document;
    //console.log(test);
    return false;
    /*
    var langvar = document.myedit.elements['fnc'];
    if (langvar != null )
        langvar.value = fncName;
    */
}
//-->
</script>

<form name="transfer" id="transfer" action="[{$oViewConf->getSelfLink()}]" method="post">
    [{$oViewConf->getHiddenSid()}]
    <input type="hidden" name="oxid" value="[{$oxid}]">
    <input type="hidden" name="cl" value="dre_voucherserie_generate">
</form>

<table cellspacing="0" cellpadding="0" border="0" width="98%">
    <tr>
        <td valign="top" class="edittext" width="355">
            <form name="myeditbatch" id="myeditbatch" action="[{$oViewConf->getSelfLink()}]" method="post">
                [{$oViewConf->getHiddenSid()}]
                <input type="hidden" name="cl" value="dre_voucherserie_generate">
                <input type="hidden" name="fnc" value="generateVoucherBatch">
                <input type="hidden" name="oxid" value="[{$oxid}]">
                <input type="hidden" name="editval[oxvoucherseries__oxid]" value="[{$oxid}]">
                <input type="hidden" name="randomNr" value="true">

                <table cellspacing="2" cellpadding="0" border="0">
                    [{block name="dre_voucherserie_generate_batch"}]
                        <tr>
                            <td class="edittext"></td>
                            <td class="edittext">
                                <strong>[{oxmultilang ident="DRE_VOUCHER_BATCH_TITLE" }]</strong>
                                [{oxinputhelp ident="HELP_DRE_VOUCHER_BATCH" }]
                            </td>
                        </tr>
                        <tr>
                            <td class="edittext"></td>
                            [{if $sGenerate_Success_Message}]
                                <td class="edittext" style="color: #00B910; padding-left: 8px">
                                    <img src="[{$oViewConf->getModuleUrl('dre_gutscheine')}]Application/views/admin/img/dre_tick.png" alt="success" title="success">
                                    [{$sGenerate_Success_Message}]
                                </td>
                            [{else}]
                                <td class="edittext"></td>
                            [{/if}]
                        </tr>
                        <tr>
                            <td class="edittext" style="vertical-align: top;" width="80">
                                [{oxmultilang ident="DRE_VOUCHER_CODES"}]
                            </td>
                            <td class="edittext" width="240" style="border-right: 1px solid #CECECE">
                                <textarea class="editinput" name="voucher_codes" cols="30" rows="5"></textarea>
                                <br/>
                                [{oxinputhelp ident="HELP_DRE_VOUCHER_CODES"}]
                            </td>
                        </tr>
                    [{/block}]
                    <tr>
                        <td class="edittext">
                        </td>
                        <td class="edittext"><br/><br/><br/>
                            <input type="submit" class="edittext" name="save" value="[{oxmultilang ident="DRE_VOUCHER_BATCH_GENERATE" }]" [{$readonly}] onClick="Javascript:changeFnc('generateVoucherBatch')">
                        </td>
                    </tr>
                </table>
            </form>
        </td>
        <td style="vertical-align: top;">
            <form name="myeditrandom" id="myeditrandom" action="[{$oViewConf->getSelfLink()}]" method="post">
                [{$oViewConf->getHiddenSid()}]
                <input type="hidden" name="cl" value="dre_voucherserie_generate">
                <input type="hidden" name="fnc" value="generateVoucherRandom">
                <input type="hidden" name="oxid" value="[{$oxid}]">
                <input type="hidden" name="editval[oxvoucherseries__oxid]" value="[{$oxid}]">
                <table cellspacing="2" cellpadding="0" border="0">
                    [{block name="dre_voucherserie_generate_random"}]
                        <tr>
                            <td class="edittext"></td>
                            <td class="edittext">
                                <strong>[{oxmultilang ident="DRE_VOUCHER_RANDOM_TITLE" }]</strong>
                                [{oxinputhelp ident="HELP_DRE_VOUCHER_RANDOM"}]
                            </td>
                        </tr>
                        <tr>
                            <td class="edittext"></td>
                            [{if $sGenerate_Random_Success_Message}]
                                <td class="edittext" style="color: #00B910; padding-left: 8px">
                                    <img src="[{$oViewConf->getModuleUrl('dre_gutscheine')}]Application/views/admin/img/dre_tick.png" alt="success" title="success">
                                    [{$sGenerate_Random_Success_Message}]
                                </td>
                            [{else}]
                                <td class="edittext"></td>
                            [{/if}]
                        </tr>
                        <tr>
                            <td class="edittext" style="vertical-align: top;" width="80">
                                [{oxmultilang ident="DRE_VOUCHER_COUNT"}]
                            </td>
                            <td class="edittext" width="195">
                                <input class="editinput" type="text" name="voucher_count" size="4" value="0">
                                [{oxinputhelp ident="HELP_DRE_VOUCHER_COUNT"}]
                            </td>
                        </tr>
                        <tr>
                            <td class="edittext" style="vertical-align: top;" width="80">
                                [{oxmultilang ident="DRE_VOUCHER_LENGTH"}]
                            </td>
                            <td class="edittext" width="195">
                                <input class="editinput" type="text" name="voucher_length" size="4" value="0">
                                [{oxinputhelp ident="HELP_DRE_VOUCHER_LENGTH"}]
                            </td>
                        </tr>
                        <tr>
                            <td class="edittext" style="vertical-align: top;" width="80">
                                [{oxmultilang ident="DRE_VOUCHER_PREFIX"}]
                            </td>
                            <td class="edittext" width="195">
                                <input class="editinput" type="text" name="voucher_prefix" size="15" value="">
                                [{oxinputhelp ident="HELP_DRE_VOUCHER_PREFIX"}]
                            </td>
                        </tr>
                        <tr>
                            <td class="edittext" style="vertical-align: top;">
                            [{oxmultilang ident="DRE_VOUCHER_CHARACTERS"}]
                            </td>
                            <td class="edittext" width="195">
                                <input class="editinput" type="text" name="voucher_characters" size="50" value="0123456789abcdefghijklmnopqrstuvwxyz">
                                [{oxinputhelp ident="HELP_DRE_VOUCHER_CHARACTERS"}]
                            </td>
                        </tr>
                    [{/block}]
                    <tr>
                        <td class="edittext">
                        </td>
                        <td class="edittext"><br>
                            <input type="submit" class="edittext" name="save" value="[{oxmultilang ident="DRE_VOUCHER_BATCH_GENERATE"}]" [{$readonly}] onClick="Javascript:changeFnc('generateVoucherRandom');">
                        </td>
                    </tr>
                </table>
            </form>
        </td>
    </tr>
</table>
<div id="dre_background" style="text-align: left; right: 5px; bottom: 5px;margin-top: 50px;">
    <a href="https://bodynova.de.de" target="_blank" title="Body-Sys">
        <img src="[{$oViewConf->getModuleUrl('dre_gutscheine')}]Application/views/admin/img/dre-sys.png" alt="Dre-Sys" title="Dre-Sys" width="5%">
    </a>
</div>
[{include file="bottomnaviitem.tpl"}]
[{include file="bottomitem.tpl"}]
