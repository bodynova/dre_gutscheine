<?php
$sLangName  = "English";

$aLang = array(
'charset'                                  => 'UTF-8',
'dre_voucherserie_generate'                => 'Generate voucher codes',
'dre_voucherserie_export'                  => 'Export voucher codes',
'DRE_VOUCHER_BATCH_GENERATE'               => 'Generate codes',
'DRE_VOUCHER_CODES'                        => 'Codes:',
'HELP_DRE_VOUCHER_CODES'                   => 'Enter all Codes.<br>
                                               Example:<br>
                                               123456<br>
                                               9876<br>
                                               code07G7<br>
                                               <br>
                                                After that click <strong>Generate codes</strong>!',
'DRE_VOUCHER_LENGTH'                       => 'Length of voucher code',
'HELP_DRE_VOUCHER_LENGTH'                  => 'The Length of the voucher code.',
'DRE_VOUCHER_COUNT'                        => 'Count of codes',
'HELP_DRE_VOUCHER_COUNT'                   => 'How many codes will be generated',
'DRE_VOUCHER_CHARACTERS'                   => 'used chars',
'HELP_DRE_VOUCHER_CHARACTERS'              => 'Which chars are used in the code',
'DRE_VOUCHER_BATCH_TITLE'                  => 'Batch generation',
'DRE_VOUCHER_RANDOM_TITLE'                 => 'Pattern generation',
'HELP_DRE_VOUCHER_RANDOM'                  => 'Which chars are used in the code',
'HELP_DRE_VOUCHER_BATCH'                   => 'You can enter several codes which are generated in one iteration.',
'DRE_VOUCHER_PREFIX'                       => 'Code prefix',
'HELP_DRE_VOUCHER_PREFIX'                  => 'The Code consists of prefix and code',
'DRE_VOUCHER_EXPORT_USED'                  => 'Export used vouchers',
'DRE_VOUCHER_GENERATED'                    => ' codes successfully generated!',
'DRE_VOUCHER_NO_ORDERS'                    => 'There is no order with this voucher series!',
);

