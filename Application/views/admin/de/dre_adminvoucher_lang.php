<?php

$sLangName  = "Deutsch";
$aLang = array(
'charset'                                  => 'UTF-8',
'tbcl_dre_voucherserie_generate'           => 'Gutscheine generieren',
'tbcl_dre_voucherserie_export'             => 'Gutscheine exportieren',
'DRE_VOUCHER_BATCH_GENERATE'               => 'Codes generieren',
'DRE_VOUCHER_CODES'                        => 'Codes:',
'HELP_DRE_VOUCHER_CODES'                   => 'Geben Sie hier alle Codes getrennt durch einen Zeilenumbruch ein.<br>
                                               Beispiel:<br>
                                               123456<br>
                                               9876<br>
                                               code07G7<br>
                                               <br>
                                                Danach klicken Sie auf <strong>Codes generieren</strong>!',
'DRE_VOUCHER_LENGTH'                       => 'Gutscheinlänge',
'HELP_DRE_VOUCHER_LENGTH'                  => 'Geben Sie an wieviele Zeichen der Gutscheincode haben soll. Der Präfix wird hier nicht mitgezählt.',
'DRE_VOUCHER_COUNT'                        => 'Gutscheinanzahl',
'HELP_DRE_VOUCHER_COUNT'                   => 'Geben Sie an wieviele Gutscheine generiert werden sollen',
'DRE_VOUCHER_CHARACTERS'                   => 'verwendete Zeichen',
'HELP_DRE_VOUCHER_CHARACTERS'              => 'Welche Zeichen sollen im Gutscheincode enthalten sein',
'DRE_VOUCHER_BATCH_TITLE'                  => 'Batchgenerierung',
'DRE_VOUCHER_RANDOM_TITLE'                 => 'Mustergenerierung',
'HELP_DRE_VOUCHER_RANDOM'                  => 'Geben Sie die Zeichen an, welche in einem Code enthalten sein dürfen und generieren Sie die Codes nach Ihren Vorgaben. ',
'HELP_DRE_VOUCHER_BATCH'                   => 'Sie können mehrere Codes angeben und in einem Durchlauf generieren lassen.',
'DRE_VOUCHER_PREFIX'                       => 'Gutschein-Pr&auml;fix',
'HELP_DRE_VOUCHER_PREFIX'                  => 'Der Gutschein-Code setzt sich zusammen aus Pr&auml;fix gefolgt vom generierten Code',
'DRE_VOUCHER_EXPORT_USED'                  => 'Eingelöste Gutscheine exportieren',
'DRE_VOUCHER_GENERATED'                    => ' Codes erfolgreich generiert!',
'DRE_VOUCHER_NO_ORDERS'                    => 'Keine Bestellungen mit dieser Gutscheinserie vorhanden!',
'VOUCHERSERIE_MAIN_NOSKIPDISCOUNT'         => 'Gutschein immer berechnen',
'HELP_VOUCHERSERIE_MAIN_NOSKIPDISCOUNT'    => 'Gutschein wird immer berechnet, auch wenn bei dem Artikel die skip Discount Flag gesetzt ist.'

);

